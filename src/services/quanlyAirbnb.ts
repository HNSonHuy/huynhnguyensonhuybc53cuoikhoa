import { apiInstance } from "constant";
import { airbnbRoom } from "types/QuanLyAirbnb";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_PHONG,
});

export const quanLyAirbnbServices = {
  getRoomList: () => api.get<ApiRespose<airbnbRoom[]>>("/phong-thue"),
  getRoomCommentList: () => api.get<ApiRespose<airbnbRoom[]>>("/binh-luan"),

};
