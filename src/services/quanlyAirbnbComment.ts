import { apiInstance } from "constant";
import { myCustomDataType } from "types/QuanlyComment";
const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_COMMENT,
});

export const quanLyAirbnbCommentServices = {
  getRoomCommentList: () => api.get<ApiRespose<myCustomDataType[]>>("/binh-luan"),
  postComment :  (payload :any) => api.post<ApiRespose<myCustomDataType>>('/binh-luan',payload)

};
