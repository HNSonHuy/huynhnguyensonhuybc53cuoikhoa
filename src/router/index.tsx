import { RouteObject } from "react-router-dom";
import { AuthLayout, Header, HeaderDetailPage } from "components/layouts";
import Login from "pages/Login";
import Register from "pages/Register";
import Account from "pages/Acount";
import { PATH } from "constant";
import Users from "pages/Admin/Users";
import Children from "pages/Children";
import Sidebar from "pages/SideBar";
import Posts from "pages/Post";
import Rooms from "pages/Rooms";
import { HomeAirbnb } from "pages";
import HeaderListRoom from "components/layouts/HeaderListRoom";
import ListRoom from "pages/ListRoom";
import AddUsers from "pages/Admin/AddUsers";
import AirBnbDetail from "pages/airbnbDetail/AirBnbDetail";
import Footer from "components/layouts/Footer";
import Location from "pages/Location";
import Notfound from "components/layouts/Notfound";
export const router: RouteObject[] = [
  {
    path: "/account",
    element: <Account />,
  },
  {
    path: PATH.admin,
    element: <Sidebar children={<Children />} />,
    children: [
      {
        path: "/admin/Post",
        element: <Posts />,
      },
      {
        path: PATH.adduser,
        element: <AddUsers />,
      },
      {
        path: PATH.users,
        element: <Users />,
      },
      {
        path: "/admin/location",
        element: <Location />,
      },
      {
        path: "/admin/rooms",
        element: <Rooms />,
      },
    ],
  },

  {
    element: <AuthLayout />,
    children: [
      {
        path: "/login",
        element: <Login />,
      },
      {
        path: "/register",
        element: <Register />,
      },
    ],
  },
  {
    element: <Header />,
    children: [
      {
        path: "/",
        element: <HomeAirbnb />,
      },
    ],
  },
  {
    element: <HeaderDetailPage />,
    children: [
      {
        path: "/AirBnbDetail/:id",
        element: <AirBnbDetail />,
      },
    ],
  },

  {
    element: <HeaderListRoom />,
    children: [
      {
        path: "/ListRoom",
        element: <ListRoom />,
      },
      {
        path: "/ListRoom/:id",
        element: <ListRoom />,
      },
    ],
  },

  {
    path: "/footer",
    element: <Footer />,
  },
  {
    path: "*",
    element: <Notfound />,
  },
];
