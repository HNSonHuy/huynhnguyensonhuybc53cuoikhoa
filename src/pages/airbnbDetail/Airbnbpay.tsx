import { DownOutlined, FlagOutlined } from "@ant-design/icons";
import { DatePicker, Dropdown, MenuProps } from "antd";
import dayjs from "dayjs";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { RootState, useAppDispath } from "store";
import { getAirbnbListThunk } from "store/quanLyAirbnb/thunk";
import { getAirbnbCommentThunk } from "store/quanLycomment/thunk";

const Airbnbpay = () => {
  // checkin-checkout
  const [khach, setKhach] = useState(1);
  const [pet, setpet] = useState(0);
  const { setValue } = useForm({ mode: "onChange" });
  const [selectedDate, setSelectedDate] = useState(dayjs());
  console.log("setSelectedDate",setSelectedDate);
  const [checkInDate, setCheckInDate] = useState(null);
  const [checkOutDate, setCheckOutDate] = useState(null);
  const [numberOfDays, setNumberOfDays] = useState(0);

  const handleDateChange = (date) => {
    if (!checkInDate) {
      setCheckInDate(date);
    } else if (!checkOutDate && date.diff(checkInDate, "day") > 0) {
      setCheckOutDate(date);

      const daysDiff = date.diff(checkInDate, "day");
      setNumberOfDays(daysDiff);
    } else {
      setCheckInDate(date);
      setCheckOutDate(null);
      setNumberOfDays(0);
    }
  };

  const items: MenuProps["items"] = [
    {
      key: "1",
      label: (
        <div className="flex justify-between">
          <div>
            <p className="text-black font-semibold">Adults</p>
            <p>Age 13+</p>
          </div>
          <div className="flex justify-center items-center">
            <button
              className="border-2 border-gray-400 rounded-md px-3"
              onClick={() => {
                if (khach > 1) {
                  setKhach(khach - 1);
                }
              }}
            >
              -
            </button>
            <p className="p-5">{khach}</p>
            <button
              className="border-2 border-gray-400 rounded-md px-3"
              onClick={() => {
                setKhach(khach + 1);
              }}
            >
              +
            </button>
          </div>
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <div className="flex justify-between">
          <div>
            <p className="text-black font-semibold">Children</p>
            <p>Ages 2–12</p>
          </div>
          <div className="flex justify-center items-center">
            <button
              className="border-2 border-gray-400 rounded-md px-3"
              onClick={() => {
                if (khach > 1) {
                  setKhach(khach - 1);
                }
              }}
            >
              -
            </button>
            <p className="p-5">{khach}</p>
            <button
              className="border-2 border-gray-400 rounded-md px-3"
              onClick={() => {
                setKhach(khach + 1);
              }}
            >
              +
            </button>
          </div>
        </div>
      ),
    },
    {
      key: "3",
      label: (
        <div className="flex justify-between">
          <div>
            <p className="text-black font-semibold">Children</p>
            <p>Ages 2–12</p>
          </div>
          <div className="flex justify-center items-center">
            <button
              className="border-2 border-gray-400 rounded-md px-3"
              onClick={() => {
                if (pet > 1) {
                  setpet(pet - 1);
                }
              }}
            >
              -
            </button>
            <p className="p-5">{pet}</p>
            <button
              className="border-2 border-gray-400 rounded-md px-3"
              onClick={() => {
                setpet(pet + 1);
              }}
            >
              +
            </button>
          </div>
        </div>
      ),
    },
  ];

  //  Map api
  const dispatch = useAppDispath();
  useEffect(() => {
    dispatch(getAirbnbListThunk());
    dispatch(getAirbnbCommentThunk());
  }, [dispatch]);
  const { id } = useParams();
  const { AirbnbList } = useSelector((state: RootState) => state.quanLyAirbnb);
  const [currentRoom, setCurrentRoom] = useState(null);
  useEffect(() => {
    if (id) {
      const roomData = AirbnbList.find((room) => room.id === parseInt(id));
      setCurrentRoom(roomData);
      setValue("maPhong", id);
    }
  }, [id, AirbnbList]);

  const handleBooking = () => {
    if (localStorage.getItem("token")) {
      if (!checkInDate || !checkOutDate) {
        toast.error("Vui lòng chọn ngày check-in và ngày check-out trước khi đặt vé.");
        return;
      } else {
        toast.success("Đặt vé thành công!", {
          position: "top-right",
          autoClose: 3000,
        });
      }
    } else {
      toast.error("Bạn cần tạo tài khoản trước khi mua vé");
    }
  };

  return (
    <>
      <div>
        <section className="">
          <div className=" p-5 shadow-xl rounded-2xl border-2 mt-[30px]   ">
            <h2 className="text-left font-semibold text-3xl  ">
              ${currentRoom?.giaTien} <span className="text-gray-600 text-lg">/night</span>
            </h2>
            <div className="text-gray-600 ">
              <div className="border-[1px] mt-5 text-xs rounded-xl border-gray-600 ">
                {/*  */}
                <div>
                  <div className="border-b-[1px]">
                    <p className="px-5 pt-3 text-red-500">*Please select your booking date</p>
                    <div className="flex justify-between  ">
                      <div className="flex p-5">
                        <DatePicker
                          style={{
                            width: "200px",
                          }}
                          className="border border-gray-200"
                          value={selectedDate}
                          onChange={handleDateChange}
                          disabledDate={(currentDate) => {
                            const today = dayjs();

                            return today.diff(currentDate, "day") > 0;
                          }}
                        />
                      </div>
                    </div>
                    <div className="flex items-center mx-auto">
                      {checkInDate && <p className="p-5 ">Ngày check-in: {checkInDate.format("DD/MM/YYYY")}</p>}
                      {checkOutDate && <p className="p-5 ">Ngày check-out: {checkOutDate.format("DD/MM/YYYY")}</p>}
                    </div>
                  </div>
                </div>
                <Dropdown menu={{ items }}>
                  <div onClick={(e) => e.preventDefault()} className="justify-center p-5 py-5 cursor-pointer">
                    <button className="text-black">GEUSTS</button>
                    <div className="flex justify-between">
                      <div className="flex">
                        <p>1</p> <span>guest</span>
                      </div>
                      <DownOutlined />
                    </div>
                  </div>
                </Dropdown>
              </div>
              <div>
                <button
                  onClick={handleBooking}
                  className="w-full bg-[#222222] hover:bg-black transition-all duration-300 rounded-xl p-5 mt-5 text-white"
                >
                  Checkout
                </button>
                <p className="text-center p-5">You won't be charged yet</p>
              </div>
              <div>
                <div className="flex justify-between pb-5 ">
                  <p>Weekend Discount </p>
                  <p>$-2</p>
                </div>
                <div className="flex justify-between pb-5">
                  <p>Service Fee</p>
                  <p>$4</p>
                </div>

                <div className="flex items-center">
                  <p> {numberOfDays > 0 && <p>Your book: {numberOfDays} days</p>}</p>
                </div>

                <div className="flex justify-between py-5 border-t-[1px] text-black ">
                  {checkInDate && checkOutDate && (
                    <h2 className="text-left font-semibold text-3xl text-green-500">
                      ${currentRoom?.giaTien * numberOfDays}{" "}
                      <span className="text-lg">
                        for <span >{numberOfDays}</span> night{numberOfDays > 1 ? "s" : ""}
                      </span>
                    </h2>
                  )}
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* Report this listing */}
        <section>
          <div className="p-5 flex lg:justify-center cursor-pointer">
            <FlagOutlined />
            <p className="pl-5 text-center">Report this listing</p>
          </div>
        </section>
      </div>
    </>
  );
};

export default Airbnbpay;
