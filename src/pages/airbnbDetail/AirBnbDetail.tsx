import { SafetyOutlined, StarOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import dayjs from "dayjs";
import "dayjs/locale/zh-cn";
import dayLocaleData from "dayjs/plugin/localeData";
dayjs.extend(dayLocaleData);
import { Map, GoogleApiWrapper } from "google-maps-react";
import { useParams } from "react-router-dom";
import { RootState } from "store";
import { useSelector } from "react-redux";

import Airbnbpay from "./Airbnbpay";
import IntroduceArbnb from "./IntroduceArbnb";
import AirbnbComment from "./AirbnbComment";
import Footer from "components/layouts/Footer";
const AirBnbDetail = (props) => {
  const [currentRoom, setCurrentRoom] = useState(null);
  const { AirbnbList } = useSelector((state: RootState) => state.quanLyAirbnb);

  const { id } = useParams();

  useEffect(() => {
    if (id) {
      const roomData = AirbnbList.find((room) => room.id === parseInt(id));
      setCurrentRoom(roomData);
    }
  }, [id, AirbnbList]);

  return (
    <div className="">
      <div className="flex py-[50px] overflow-hidden font-semibold items-center m-auto justify-between ">
        <div className="">
          <div className="w-[60%] mx-auto max-laptop:w-[80%] max-tablet:w-[90%]">
            <section className=" max-tablet:ml-[10px] ">
              <div className=" my-2 ">
                <h2 className="mb-5 font-semibold max-ipad:text-[20px] text-3xl">
                  {currentRoom?.tenPhong}
                </h2>
                <img
                  className="rounded-3xl shadow-lg"
                  src={currentRoom?.hinhAnh}
                  alt="../"
                />
              </div>
            </section>

            <section >
             
              <div className="flex max-mdDesk:flex-col">
                <div className="max-mdDesk:w-full w-2/3">
                  <IntroduceArbnb />
                </div>

                <div className=" w-1/3 max-mdDesk:w-full  ">
                  <Airbnbpay />
                </div>
              </div>
            </section>

            <div className="py-10 border-b-[1px]">
              <AirbnbComment />
            </div>
          </div>

          <div className="relative left-[20%] max-ip678Plus:ml-[10px] max-laptop:left-[10%] max-tablet:w-[90%] lg:w-[60%] max-ipad:w-[80%]  max-tablet:left-[5%] max-ip678Plus:left-[2.5%]">
            <div className=" py-5 border-b-[1px] ">
              <div>
                <p className="mb-5 text-xl">Your Airbnb location</p>
              </div>
              <div className="h-[500px]  relative">
                <Map
                  google={props.google}
                  initialCenter={{
                    lat: 34.0522,
                    lng: -118.2437,
                  }}
                />
              </div>
              <p className="mb-5 ">Lumban, Calabarzon, Philippines</p>
              <p className="text-gray-600 mb-5">
                24063 Dunlap Rd. Rockbridge, OH 43149
              </p>
              <button className="underline">Show more</button>
            </div>
            <div className=" py-5 text-left border-gray-200 text-gray-600 ">
              <div className="flex max-tablet:flex-col ">
                <img
                  className="w-24 rounded-full my-5"
                  src="https://a0.muscache.com/im/pictures/user/84d63cb0-42a9-4e68-a7d9-10d2b29f95ee.jpg?im_w=240"
                  alt=".../"
                />

                <div className="ml-5 my-5 ">
                  <p className="font-semibold text-3xl text-left max-tablet:text-[20px] mb-2 text-black">
                    Hosted by Camper And Cabin
                  </p>
                  <p>Superhost3 years hosting</p>
                </div>
              </div>
              <div className="flex pb-5">
                <div className="flex">
                  <div className="mr-5 h-7">
                    <StarOutlined />
                  </div>
                  <p>481 Reviews</p>
                </div>

                <div className="flex ml-5">
                  <div className="mr-5 h-7">
                    <SafetyOutlined />
                  </div>
                  <p>481 Reviews</p>
                </div>
              </div>
              <div>
                <p className="pb-5">Response rate: 27%</p>
                <p className="pb-5">Response time: a few days or more</p>
                <button className="border-[1px] border-black rounded-lg py-3 px-7 hover:bg-mainColor hover:border-mainColor duration-200 hover:text-white">
                  Contact Host
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default GoogleApiWrapper({
  apiKey: "AIzaSyADU0JtRsZXZPVgDIfpWFOa8oE5JFz5YeQ",
})(AirBnbDetail);
