import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { useAppDispath } from "store";
import { getAirbnbListThunk } from "store/quanLyAirbnb/thunk";
import { RootState } from "store";
import { getAirbnbCommentThunk } from "store/quanLycomment/thunk";
import { useForm } from "react-hook-form";

const IntroduceArbnb = () => {
  const { setValue } = useForm({ mode: "onChange" });
  const { id } = useParams();
  const dispatch = useAppDispath();

  // Map api
  const [currentRoom, setCurrentRoom] = useState(null);
  useEffect(() => {
    dispatch(getAirbnbListThunk());
    dispatch(getAirbnbCommentThunk());
  }, [dispatch]);

  const { AirbnbList } = useSelector((state: RootState) => state.quanLyAirbnb);
  useEffect(() => {
    if (id) {
      const roomData = AirbnbList.find((room) => room.id === parseInt(id));
      setCurrentRoom(roomData);
      setValue("maPhong", id);
    }
  }, [id, AirbnbList]);

  return (
    <>
      <div>
        {/* Room-name */}
        <section className="">
          <div className=" mt-[30px]">
            <div>
              <h2 className="font-semibold  text-1xl">{currentRoom?.tenPhong}</h2>
            </div>

            {/*  4 chưc nang phong */}
            <section className="">
              <div className="border-gray-200 border-b-[1px] py-5 grid grid-cols-4 max-MaxDesk:grid-cols-3 max-tablet:grid-cols-2 z ">
                <div className="flex mb-5 w-36 px-6 py-3 border-2 rounded-xl">
                  <img className="h-9 flex mr-5" src="https://i.ibb.co/kg7DV6P/guest.png" alt=".../" />

                  <div className="">
                    <p>Guest</p>
                    <p className="text-gray-600 items-center mx-auto justify-center flex "> {currentRoom?.khach}</p>
                  </div>
                </div>

                <div className="flex w-36 mb-5 px-6 py-3 border-2 rounded-xl">
                  <img className="h-9 flex mr-5" src="https://i.ibb.co/fkcL9BP/bed.png" alt=".../" />
                  <div className="">
                    <p>Bed</p>
                    <p className="text-gray-600 items-center mx-auto justify-center flex"> {currentRoom?.phongNgu}</p>
                  </div>
                </div>

                <div className="flex w-36 mb-5 px-6 py-3 border-2 rounded-xl">
                  <img className="h-9 flex mr-5" src="https://i.ibb.co/CwTVZP4/house.png" alt=".../" />
                  <div className="">
                    <p>Bedroom</p>
                    <p className="text-gray-600 items-center mx-auto justify-center flex"> {currentRoom?.phongNgu}</p>
                  </div>
                </div>

                <div className="flex w-36 mb-5 px-6 py-3 border-2 rounded-xl">
                  <img className="h-9 flex mr-5" src="https://i.ibb.co/tZtjgDJ/bath.png" alt=".../" />
                  <div className="">
                    <p>Bath</p>
                    <p className="text-gray-600 items-center mx-auto justify-center flex"> {currentRoom?.phongTam}</p>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </section>

        {/* mo ta */}
        <section className=" max-ipad:w-full">
          <div>
            <p className="my-5 text-xl">Airbnb Introduction</p>
          </div>
          <div className="border-b-[1px] py-5 leading-[30px] border-gray-200 flex">
            <p className="text-gray-600 max-tablet:hidden">{currentRoom?.moTa}</p>
            <p className="text-gray-600 max-tablet:block hidden">{currentRoom?.moTa.substring(1, 100) + "..."}</p>
          </div>
        </section>

        {/* Where you'll sleep*/}
        <section className="">
          <div className="py-5">
            <p className="mb-5 text-xl">Where you'll sleep</p>
            <div className="flex py-5 border-b-[1px]">
              <div className="rounded-xl">
                <img className="h-32 rounded-xl shadow-lg flex  " src={currentRoom?.hinhAnh} alt=".../" />
                <h2 className="my-3">Bedroom </h2>
                <p className="text-gray-600">
                  {currentRoom?.giuong} King bed • {currentRoom?.phongTam} Bathroom{" "}
                  {currentRoom?.bep ? "Kitchen" : <span>• Non-Kitchen</span>}
                </p>
              </div>
            </div>
          </div>
        </section>

        {/* What this place offers */}
        <section>
          <div>
            <p className="text-xl mb-5">What this place offers</p>
            <div className="border-b-[1px] py-5 grid grid-cols-2">
              <div className="flex mb-5">
                {currentRoom?.banLa ? (
                  <>
                    <img className="h-12 flex mr-5" src="https://i.ibb.co/hgsHdNj/ironing.png" alt=".../" />
                    <p className="items-center flex">iron</p>
                  </>
                ) : (
                  <p className="items-center flex">Non-iron</p>
                )}
              </div>

              <div className="flex mb-5">
                {currentRoom?.banUi ? (
                  <>
                    <img className="h-12 flex mr-5" src="https://i.ibb.co/mzYX4NV/iron2.png" alt=".../" />
                    <p className="items-center flex">flat iron</p>
                  </>
                ) : (
                  <p className="items-center flex">flat iron</p>
                )}
              </div>

              <div className="flex mb-5">
                {currentRoom?.dieuHoa ? (
                  <>
                    <img className="h-12 flex mr-5" src="https://i.ibb.co/WGncG66/air-conditioner.png" alt=".../" />
                    <p className="items-center flex">air condition</p>
                  </>
                ) : (
                  <p className="items-center flex">Non-air condition</p>
                )}
              </div>

              <div className="flex mb-5">
                {currentRoom?.doXe ? (
                  <>
                    <img
                      className="h-12 flex mr-5"
                      src= "https://i.ibb.co/qdpgvwn/parking.png"
                      alt=".../"
                    />
                    <p className="items-center flex">Parking</p>
                  </>
                ) : (
                  <p className="items-center flex">Non-parking</p>
                )}
              </div>

              <div className="flex mb-5">
                {currentRoom?.tivi ? (
                  <>
                    <img
                      className="h-12 flex mr-5"
                      src= "https://i.ibb.co/4YHFwwN/television.png"
                      alt=".../"
                    />
                    <p className="items-center flex">Television</p>
                  </>
                ) : (
                  <p className="items-center flex">Non-television</p>
                )}
              </div>

              <div className="flex mb-5">
                {currentRoom?.wifi ? (
                  <>
                    <img
                      className="h-12 flex mr-5"
                      src="https://i.ibb.co/HHTzsdf/wifi.png"
                      alt=".../"
                    />
                    <p className="items-center flex">Wifi</p>
                  </>
                ) : (
                  <p className="items-center flex">Non-wifi</p>
                )}
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default IntroduceArbnb;
