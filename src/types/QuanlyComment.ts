export type myCustomDataType = {
  id: number,
  maPhong: number,
  maNguoiBinhLuan: number,
  ngayBinhLuan: string,
  noiDung: string,
  saoBinhLuan: number,
};