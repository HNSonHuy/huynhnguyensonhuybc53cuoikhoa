import { createSlice } from '@reduxjs/toolkit'
import { uploadAnhThunk } from './thunk';
type Img = {
    avatar ? : string
}
const initialState :Img = {  
}

const UploadAnhSlice = createSlice({
  name: 'UploadAnh',
  initialState,
  reducers: {},
  extraReducers(builder) {
      builder.addCase(uploadAnhThunk.fulfilled,(state,{payload}) => {
        state.avatar = payload.avatar
      }) 
  },
});

export const {} = UploadAnhSlice.actions

export default UploadAnhSlice.reducer